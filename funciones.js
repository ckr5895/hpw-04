function asociar_hijo(obj_padre,obj_hijo){
    obj_padre.appendChild(obj_hijo);
}


function crear_label(nom,val){
    obj=document.createElement("label");
    obj.setAttribute("name",nom);
    obj.innerHTML = val;
    return obj;
}

function crear_input_text(nom,val){
    obj = document.createElement("input");
    obj.setAttribute("type","text");
    obj.setAttribute("name",nom);
    obj.setAttribute("value",val);
    return obj;
}

function crear_input_email(nom,value){
    obj = document.createElement("input");
    obj.setAttribute("type","email");
    obj.setAttribute("name",nom);
    obj.setAttribute("value",value);
    return obj;
}

function crear_textArea(nom,val){
    obj = document.createElement("textarea");
    obj.setAttribute("name",nom);
    obj.innerHTML = val;
    return obj;
}

function crear_input_contrasenia(nom,val){
    obj = document.createElement("input");
    obj.setAttribute("type","password");
    obj.setAttribute("name",nom);
    obj.setAttribute("placeholder",val);
    return obj;
}

function crear_radio(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","radio");
    obj.setAttribute("name",nom);
    return obj;
}

function crear_radio_check(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","radio checked");
    obj.setAttribute("name",nom);
    return obj;
}


function crear_check(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","checkbox");
    obj.setAttribute("name",nom);
    return obj;
}

function crear_option(nom,opciones){
    obj = document.createElement("select");
    obj.setAttribute("name",nom);
    for(var i=0;i < opciones.length;i++){
        obj_o = document.createElement("option");
        obj_o.innerHTML = opciones[i];
        asociar_hijo(obj,obj_o);
    }
    return obj;
}

function crear_option_gr(nom,opciones,opciones2){
    obj = document.createElement("select");
    obj.setAttribute("name",nom);
    gro1 = document.createElement("optgroup");
    gro1.setAttribute("label","Grupo 1");
    asociar_hijo(obj,gro1);
    for(var i=0;i < opciones.length;i++){
        obj_o = document.createElement("option");
        obj_o.innerHTML = opciones[i];
        asociar_hijo(gro1,obj_o);
    }
    gro2 = document.createElement("optgroup");
    gro2.setAttribute("label","Grupo 2");
    asociar_hijo(obj,gro2);
    for(var i=0;i < opciones2.length;i++){
        obj_o = document.createElement("option");
        obj_o.innerHTML = opciones2[i];
        asociar_hijo(gro2,obj_o);
    }
    return obj;
}

function crear_file(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","file");
    obj.setAttribute("name",nom);
    return obj;
}


function crear_fieldset(nom){
    obj = document.createElement("fieldset");
    obj.setAttribute("name",nom)
    leg = document.createElement("legend");
    leg.innerHTML = "Legend";
    asociar_hijo(obj,leg);
    lab=document.createElement("label");
    lab.innerHTML="Label";
    lab.setAttribute("for","field");
    asociar_hijo(obj,lab);
    inp = document.createElement("input");
    inp.setAttribute("id","field");
    asociar_hijo(obj,inp);
    return obj;
}


function crear_input_number(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","number");
    obj.setAttribute("name",nom);
    return obj;
}

function crear_input_number_min_max(nom,min,max,step){
    obj = document.createElement("input");
    obj.setAttribute("type","number");
    obj.setAttribute("name",nom);
    obj.setAttribute("min",min);
    obj.setAttribute("max",max);
    obj.setAttribute("step",step);
    return obj;
}

function crear_input_range(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","range");
    obj.setAttribute("name",nom);
    return obj;
}


function crear_input_range_min_max(nom,min,max,step){
    obj = document.createElement("input");
    obj.setAttribute("type","range");
    obj.setAttribute("name",nom);
    obj.setAttribute("min",min);
    obj.setAttribute("max",max);
    obj.setAttribute("step",step);
    return obj;
}

function crear_input_color(nom){
    obj = document.createElement("input");
    obj.setAttribute("type","color");
    obj.setAttribute("name",nom);
    return obj;
}

function crear_input_color_val(nom,val){
    obj = document.createElement("input");
    obj.setAttribute("type","color");
    obj.setAttribute("name",nom);
    obj.setAttribute("value",val);
    return obj;
}

function crear_progreso_val(nom,max){
    obj = document.createElement("progress");
    obj.setAttribute("nom",nom);
    obj.setAttribute("max",max);
    obj.setAttribute("value",50);
    return obj
}

function crear_progreso(nom){
    obj = document.createElement("progress");
    obj.setAttribute("nom",nom);
    return obj
}


function ejecutar(){
    form = document.createElement("table");
    form.setAttribute("name","Tabla");
    
    la = crear_label("label","Ejemplo de label");
    asociar_hijo(form,la);
    
    te = crear_input_text("Input","Ejemplo de inputtext");
    asociar_hijo(form,te);
    
    em=crear_input_email("Email","Ejemplo de Email");
    asociar_hijo(form,em);
    
    tar=crear_textArea("Text Area","Ejemplo de Text Area");
    asociar_hijo(form,tar);
    
    inpcon = crear_input_contrasenia("Password","Introduce contraseña");
    asociar_hijo(form,inpcon);
    
    rad = crear_radio("radio");
    asociar_hijo(form,rad);
    
    rad2 = crear_radio_check("radio");
    asociar_hijo(form,rad2);
    
    che = crear_check("check");
    asociar_hijo(form,che);
    
    opt = crear_option("opciones",["a","b","c","d"]);
    asociar_hijo(form,opt);
    
    optg = crear_option_gr("Grupos",["a","b","c","d"],[1,2,3,4,5]);
    asociar_hijo(form,optg);
    
    fil = crear_file("file");
    asociar_hijo(form,fil);
    
    file2 = crear_fieldset("field set");
    asociar_hijo(form,file2);
    
    num = crear_input_number("input number");
    asociar_hijo(form,num);
    
    num2 = crear_input_number_min_max("Max",0,100,10);
    asociar_hijo(form,num2);
    
    rang = crear_input_range("input range");
    asociar_hijo(form,rang);
    
    rang2 = crear_input_range_min_max("rango2",0,100,15);
    asociar_hijo(form,rang2);
    
    color = crear_input_color("color");
    asociar_hijo(form,color);
    
    color2 = crear_input_color_val("color 2","#e76252");
    asociar_hijo(form,color2);
    
    prog1 = crear_progreso_val("progreso 2",100);
    asociar_hijo(form,prog1);
    
    prog = crear_progreso_val("progreso",100);
    asociar_hijo(form,prog);
    
    asociar_hijo(document.body,form);
}

ejecutar();